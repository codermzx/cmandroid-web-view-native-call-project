package com.example.myapplication

import android.content.Context
import com.just.agentweb.AgentWeb
import android.webkit.JavascriptInterface
import com.example.myapplication.CMNativeInterface
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import android.widget.Toast
import com.example.myapplication.MainActivity

class CMNativeInterface(private val agent: AgentWeb, private val context: Context) {
    private val TAG = "CoderM"

    val down = object : CountDownTimer(1000, 1000) {
        //1000ms运行一次onTick里面的方法
        override fun onFinish() {
            Log.e(TAG, "倒计时结束，开始执行")
            // TODO: 2021/10/30 初始化三放库
        }

        override fun onTick(millisUntilFinished: Long) {
        }
    }

    // 这里保留一个方法,以后可以任意扩展,必须判断参数是json类型字符串
    @JavascriptInterface
    fun anyCall(jsonArgs: String, callBackName: String?) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction anyCall. args==>  jsonArgs=$jsonArgs")

        // 回调指定的方法
        agent.jsAccessEntrace.quickCallJs(callBackName, "")
    }

    @JavascriptInterface
    fun init() {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction init.")

        down.start()

        // 回调初始化方法
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_InitCallBack, "{\"code\":0,\"data\":{}}")
    }

    // 调用激励视频
    @JavascriptInterface
    fun showAdVideo(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAd_Video.  args==>  $RewardId")
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_VideoAdCallBack, backArg)
    }

    // 调用banner
    @JavascriptInterface
    fun showAdBanner(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAd_Banner.  args==>  $RewardId")
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_BannerAdCallBack, backArg)
    }

    //调用插屏
    @JavascriptInterface
    fun showAdInterstitial(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAd_Interstitial.  args==>  $RewardId")
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_InterstitialAdCallBack, backArg)
    }

    //调用闪屏
    @JavascriptInterface
    fun showAdSplash(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAdSplash.  args==>  $RewardId")
        Toast.makeText(context, "当前渠道没有开屏广告", Toast.LENGTH_LONG).show()
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_SplashAdCallBack, backArg)
    }

    //调用闪屏
    @JavascriptInterface
    fun showAdNative(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAdNative.  args==>  $RewardId")
        Toast.makeText(context, "当前渠道没有原生广告", Toast.LENGTH_LONG).show()
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_NativeAdCallBack, backArg)
    }

    //调用闪屏
    @JavascriptInterface
    fun showAdVideoFullScreen(RewardId: String) {
        // 输出参数
        Log.e(TAG, "CMNativeInterface CallFunction showAdVideoFullScreen.  args==>  $RewardId")
        Toast.makeText(context, "当前渠道没有全屏视频广告", Toast.LENGTH_LONG).show()
        val backArg = "{\"code\":0,\"data\":{\"id\":\"$RewardId\"}}"
        // 回调激励视频
        agent.jsAccessEntrace.quickCallJs(CMJSNativeFunc_VideoFullScreenAdCallBack, backArg)
    }

    companion object {
        // 初始化的回调方法
        const val CMJSNativeFunc_InitCallBack = "CMJSNativeFunc_InitCallBack"

        // 激励视频的回调方法
        const val CMJSNativeFunc_VideoAdCallBack = "CMJSNativeFunc_VideoAdCallBack"

        // banner的回调方法
        const val CMJSNativeFunc_BannerAdCallBack = "CMJSNativeFunc_BannerAdCallBack"

        // 插屏广告的回调方法
        const val CMJSNativeFunc_InterstitialAdCallBack = "CMJSNativeFunc_InterstitialAdCallBack"

        // 开屏回调
        const val CMJSNativeFunc_SplashAdCallBack = "CMJSNativeFunc_SplashAdCallBack"

        const val CMJSNativeFunc_NativeAdCallBack = "CMJSNativeFunc_NativeAdCallBack"

        const val CMJSNativeFunc_VideoFullScreenAdCallBack = "CMJSNativeFunc_VideoFullScreenAdCallBack"
    }
}